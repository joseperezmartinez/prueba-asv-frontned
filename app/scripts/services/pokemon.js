
(function(){

  'use strict';

  angular.module('testAngularApp')
    .service('PokemonService', function ($http) {

      return {
        getTypes: getTypes,
        getListing: getListing,
        getOne: getOne,
        create: create,
        update: update,
        updateFavorite: updateFavorite,
        remove: remove
      };

      function getTypes() {
        return $http.get(POKEEMON_TYPES_URL);
      }

      function getListing(page, limit) {
        var url = POKEEMON_LIST_URL
          .replace(':page', page - 1)
          .replace(':limit', limit);

        return $http.get(url);
      }

      function getOne(pokemonId) {
        var url = POKEEMON_ITEM_URL.replace(':pokemonId', pokemonId);
        return $http.get(url);
      }

      function create(data) {
        console.log(data);
        return $http.post(POKEEMON_CREATE_URL, data);
      }

      function update(pokemonId, data) {
        var url = POKEEMON_ITEM_URL.replace(':pokemonId', pokemonId);
        return $http.put(url, data);
      }

      function updateFavorite(data) {
        var url = POKEEMON_ITEM_URL.replace(':pokemonId', data.id);
        return $http.patch(url, data);
      }

      function remove(pokemonId) {
        var url = POKEEMON_ITEM_URL.replace(':pokemonId', pokemonId);
        return $http.delete(url);
      }

    });

})();
