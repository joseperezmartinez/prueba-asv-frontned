
const API_HTTP_PROTO = 'http';
const API_HTTP_PORT = 8585;
const API_HOSTNAME = 'localhost';
const API_HOST = API_HTTP_PROTO + '://' + API_HOSTNAME + ':' + API_HTTP_PORT;
const ITEMS_PER_PAGE = 10;
const POKEMON_BASE_URL = '/pokemon';
const POKEEMON_TYPES_URL = API_HOST + POKEMON_BASE_URL + '/types';
const POKEEMON_LIST_URL = API_HOST + POKEMON_BASE_URL + '?page=:page&limit=:limit';
const POKEEMON_ITEM_URL = API_HOST + POKEMON_BASE_URL + '/:pokemonId';
const POKEEMON_CREATE_URL = API_HOST + POKEMON_BASE_URL;
