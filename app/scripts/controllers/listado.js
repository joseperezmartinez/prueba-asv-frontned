'use strict';

/**
 * @ngdoc function
 * @name testAngularApp.controller:ListadoCtrl
 * @description
 * # MainCtrl
 * Controller of the testAngularApp
 */
angular.module('testAngularApp')
  .controller('ListadoCtrl', function ($log, PokemonService) {
    var self = this;

    $log.log('Entron en ListadoCtrl');

    // Filtros de la tabla
    self.orderBy = 'name';
    self.reverse = false;
    self.currentPage = 1;
    self.itemsPerPage = 5;
    self.totalItems = 0;
    self.listado = [];

    // Callbacks
    self.setOrdrByColumn = function (field) {
      if (self.orderBy == field) {
        self.reverse = !self.reverse;
      }
      self.orderBy = field;
      $log.log('Cambio orden. Col: %s', field);
    };

    self.setPage = function (pageNo) {
      self.currentPage = pageNo;
    };

    self.pageChanged = function () {
      $log.log('Page changed to: ' + self.currentPage);
      loadPageElements();
    };

    self.setItemsPerPage = function (num) {
      self.itemsPerPage = num;
      self.currentPage = 1; //reset to first page
    };

    self.updateFavorite = function (item) {
      var temp = jQuery.extend({}, item);
      temp.favorite = !item.favorite;

      PokemonService
        .updateFavorite(temp)
        .then(function(response) {
          item.favorite = temp.favorite;
          $log.log('Favorite flag [%s] updsted for %s', item.favorite, item.id);
        })
        .catch(function(response) {
          $log.error('HTTP Error! ', response);
        });
    };

    function loadPageElements() {
      PokemonService
        .getListing(self.currentPage, self.itemsPerPage)
        .then(function(response) {
          self.listado = response.data.content;
          self.totalItems = response.data.totalElements;
        })
        .catch(function(response) {
          $log.error('HTTP Error! ', response);
        });
    }

    function init() {
      self.currentPage = 1;
      self.itemsPerPage = ITEMS_PER_PAGE;
      loadPageElements();
    }

    init();

  });
