/**
 * @ngdoc function
 * @name testAngularApp.controller:EditarCtrl
 * @description
 * # MainCtrl
 * Controller of the testAngularApp
 */
(function () {

  'use strict';

  angular.module('testAngularApp')
    .controller('EditarCtrl', function ($routeParams, $location, PokemonService) {

      var self = this;

      self.pokemon_id = $routeParams.pokemon_id;
      self.data = {};

      // callbacks
      self.volver = volver;
      self.guardar = guardar;
      self.eliminar = eliminar;

      function eliminar() {
        if (confirm('¿Seguro que deseas borrar este pokemon?')) {
          PokemonService
            .remove(self.data.id)
            .then(function (response) {
              volver();
            })
            .catch(function (response) {
              $log.error('HTTP Error! ', response);
            });
        }
      }

      function volver() {
        $location.path('/');
      }

      function guardar() {
        PokemonService
          .update(self.data.id, self.data)
          .then(function (response) {
            alert('Pokemon editado OK');
          })
          .catch(function (response) {
            $log.error('HTTP Error! ', response);
          });
      }

      function init() {
        PokemonService
          .getTypes()
          .then(function (response) {
            self.types = response.data;
            console.log(self.types);

            PokemonService
              .getOne(self.pokemon_id)
              .then(function (response) {
                self.data = response.data;
                console.log(self.data);
              });
          })
          .catch(function (response) {
            $log.error('HTTP Error! ', response);
          });
      }

      init();

    });

})();

