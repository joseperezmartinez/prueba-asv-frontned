
/**
 * @ngdoc function
 * @name testAngularApp.controller:CrearCtrl
 * @description
 * # MainCtrl
 * Controller of the testAngularApp
 */
(function(){

  'use strict';

  angular.module('testAngularApp')
    .controller('CrearCtrl', function ($location, PokemonService) {

      var self = this;
      self.types = [];
      self.pokemons = [];

      self.data = {
        id: 0,
        name: '',
        description: '',
        firstType: null,
        secondType: null,
        evolvesTo: null,
        favorite: false
      };

      // callbacks
      self.volver = volver;
      self.guardar = guardar;

      function volver() {
        $location.path('/');
      }

      function guardar() {
        PokemonService
          .create(self.data)
          .then(function (response) {
            alert('Pokemon creado OK');
          })
          .catch(function (response) {
            $log.error('HTTP Error! ', response);
          });
      }

      function init() {
        PokemonService
          .getTypes()
          .then(function (response) {
            self.types = response.data;
            console.log(self.types);
          })
          .catch(function (response) {
            $log.error('HTTP Error! ', response);
          });
        PokemonService
          .getListing(1, 100)
          .then(function (response) {
            self.pokemons = response.data.content;
          });
      }

      init();

    });

})();
