
(function() {

  'use strict';

  /**
   * @ngdoc overview
   * @name testAngularApp
   * @description
   * # testAngularApp
   *
   * Main module of the application.
   */
  angular
    .module('testAngularApp', [
      'ngRoute',
      'ui.bootstrap',
      'ui.bootstrap.pagination'
    ])
    .filter('capitalize', function() {
      return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
      };
    })
    .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'views/listado.html',
          controller: 'ListadoCtrl',
          controllerAs: 'vm'
        })
        .when('/crear-pokemon', {
          templateUrl: 'views/crear.html',
          controller: 'CrearCtrl',
          controllerAs: 'vm'
        })
        .when('/editar-pokemon/:pokemon_id', {
          templateUrl: 'views/editar.html',
          controller: 'EditarCtrl',
          controllerAs: 'vm'
        })
        .otherwise({
          redirectTo: '/'
        });
    });

})();
