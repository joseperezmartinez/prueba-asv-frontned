# Prueba ASV Frontend

Frontend AngularJS 1.6.9 para la prueba fullstack para ASV.

## Despliegue

* Requisitos son bower y npm.

- Descsrgar el proyecto
- Descargar dependencias.
  - ``bower install``
  - ``npm install``
  - Configurar parametros del servidor REST en el fichero app/scripts/constants.js.
- Se puede ejecutar con un simple ``grunt serve``
- Se puede instalar en una máquina. En la carpeta destino copiar el contenido de la carpeta app y copiar completa la carpeta descargada bower_components.  El servidor web a elección: apache2, etc.

